import {Component, EventEmitter, Output} from '@angular/core';
import {BoxItem} from "../../core/models/box-item.model";
import {Camera, CameraOptions} from "@ionic-native/camera";

@Component({
  selector: 'create-box-item',
  templateUrl: 'create-box-item.html'
})
export class CreateBoxItemComponent {

  itemName: string;
  itemQuantity: number = 1;
  itemPhoto: string;

  @Output() onCreate = new EventEmitter<BoxItem>();

  constructor(private camera: Camera,) {
  }

  /**
   * Create box item object and emit with output emitter.
   */
  storeItem() {
    let boxItem: BoxItem = {
      name: this.itemName,
      quantity: this.itemQuantity,
      photo: this.itemPhoto,
    };
    this.onCreate.emit(boxItem);
  }

  /**
   * Take picture from camera and assign received base64 data to photo property.
   */
  takePicture() {

    //@todo: handle images size and aspect ratio.
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 200,
      targetHeight: 200,
    };

    this.camera.getPicture(options).then((imageData) => {

      this.itemPhoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
      //@todo: handle error
    });
  }
}
