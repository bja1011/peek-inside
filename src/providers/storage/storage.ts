import {Injectable} from '@angular/core';
import {Storage} from "@ionic/storage";

@Injectable()
export class StorageService {

  constructor(private storage: Storage) {
  }

  /**
   * Store data with key and value.
   * @param {string} key
   * @param value
   * @returns {Promise<any>}
   */
  setItem(key: string, value: any) {
    return this.storage.set(key, value);
  }

  /**
   * Get item with key.
   * @param {string} key
   * @returns {Promise<any>}
   */
  getItem(key: string) {
    return this.storage.get(key);
  }

  /**
   * Find items by searching {keyPrefix} in item {key}.
   * @param {string} keyPrefix
   * @returns {StorageItem[]}
   */
  findItems(keyPrefix: string) {
    return new Promise((resolve) => {
      let results: StorageItem[] = [];
      this.storage.forEach((value, key) => {
        if (key.indexOf(keyPrefix) > -1) {
          results.push({
            key,
            value
          });
        }
      })
        .then(() => {
          resolve(results);
        })
    })
  }
}

export interface StorageItem {
  key: string;
  keyId?: number;
  value: any;
}
