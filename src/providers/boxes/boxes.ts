import {Injectable} from '@angular/core';
import {StorageItem, StorageService} from "../storage/storage";
import {Box} from "../../core/models/box.model";
import {STORAGE_BOX_PREFIX_NAME} from "../../constants/app.constants";

@Injectable()
export class BoxesService {

  constructor(public storageService: StorageService,) {
  }

  /**
   * Store box data to storage.
   * @param {Box} boxData
   * @returns {Promise<any>}
   */
  storeBox(boxData: Box) {
    return this.storageService.setItem(`${STORAGE_BOX_PREFIX_NAME}-${boxData.id}`, boxData);
  }

  /**
   * Get box from storage.
   * @param {number} boxId
   * @returns {Promise<any>}
   */
  getBox(boxId: number) {
    return this.storageService.getItem(`${STORAGE_BOX_PREFIX_NAME}-${boxId}`);
  }

  /**
   * Get all boxes.
   * @returns {Promise<Box[]>}
   */
  getAllBoxes() {
    return this.storageService.findItems('box')
      .then((boxes: StorageItem[]) => {
        return boxes.map((box: StorageItem) => {
          return box.value;
        })
      })
  }

  /**
   * Get all boxes locations
   * @returns {Promise<any>}
   */
  getBoxLocations() {
    return this.storageService.getItem('locations');
  }

  /**
   * Store box location in storage.
   * @param {string} locationName
   * @returns {Promise<any>}
   */
  storeBoxLocation(locationName: string) {
    return new Promise((resolve, reject) => {
      this.storageService.getItem('locations')
        .then(locations => {
          if (!locations) locations = [];
          if (locations.indexOf(locationName) == -1) {
            locations.push(locationName);
            this.storageService.setItem('locations', locations)
              .then(
                data => {
                  resolve(data)
                },
                (err) => {
                  reject(err)
                })
          }
        })
    });
  }
}