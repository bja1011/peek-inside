import {Injectable} from '@angular/core';
import {BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult} from "@ionic-native/barcode-scanner";
import {Platform} from "ionic-angular";

@Injectable()
export class QRScannerService {

  private unregisterBackButtonAction: Function;

  constructor(private barcodeScanner: BarcodeScanner,
              private platform: Platform,) {
  }

  /**
   * Show native barcode scanner and return scan results as promise.
   * @param {BarcodeScannerOptions} customOptions - Scanner options object.
   * @returns {Promise<BarcodeScanResult>}
   */
  public scan(customOptions?: BarcodeScannerOptions): Promise<BarcodeScanResult> {

    const defaultOptions: BarcodeScannerOptions = {
      formats: 'QR_CODE',
      showTorchButton: false,
    };

    // Merge default with passed options.
    const options: BarcodeScannerOptions = {
      ...defaultOptions,
      ...customOptions
    };

    // Register back button handler.
    this.registerBackButtonHandler();

    return new Promise((resolve, reject) => {
      this.barcodeScanner.scan(options)
        .then(barcodeData => {
          // Check if QR code text has `pibox` string and override return text to id.
          if (barcodeData.text.indexOf('pibox') > -1) {
            barcodeData.text = barcodeData.text.split('-')[1];
            resolve(barcodeData);
          } else {
            reject({
              error: 'no-pattern',
              errorText: 'Correct boxId not found.'
            });
          }

          // Unregister back button with delay
          setTimeout(() => {
            this.unregisterBackButtonAction && this.unregisterBackButtonAction();
          }, 500)
        })
        .catch(err => {
          reject(err);
          console.log('Error', err);
        });
    });
  }

  /**
   * Register callback for back button clicked to avoid app exit.
   */
  private registerBackButtonHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      // Nothing to do, just disable default back button action.
    }, 101);
  }

}
