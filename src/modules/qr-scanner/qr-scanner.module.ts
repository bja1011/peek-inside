import {NgModule} from '@angular/core';
import {BarcodeScanner, BarcodeScannerOptions, BarcodeScanResult} from "@ionic-native/barcode-scanner";
import {QRScannerService} from "./providers/qr-scanner.service";
import {BARCODE_SCAN_RESULT_MOCK} from "../../constants/mocks";

/**
 * Extend BarcodeScanner class and override scan method to mock data if browser platform.
 * @todo: implement html5 qr scanner.
 */
export class MockBarcodeScanner extends BarcodeScanner {
  scan(options?: BarcodeScannerOptions): Promise<BarcodeScanResult> {
    return new Promise((resolve) => {
      resolve({...BARCODE_SCAN_RESULT_MOCK})
    });
  }
}
const isCordovaApp = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;

@NgModule({
  providers: [
    QRScannerService,
    {
      provide: BarcodeScanner,
      useClass: isCordovaApp ? BarcodeScanner : MockBarcodeScanner
    },
  ],
  imports: [],
  exports: []
})
export class QRScannerModule {
}
