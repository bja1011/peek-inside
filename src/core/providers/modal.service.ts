import {Injectable} from "@angular/core";
import {AlertController, AlertOptions, ModalController} from "ionic-angular";

@Injectable()
export class ModalService {

  constructor(private modalController: ModalController,
              private alertController: AlertController,) {
  }

  /**
   * Open modal with passed component.
   * @param component - Angular component to render in modal.
   * @param data - Data will be passed to rendered component with navParams.
   * @returns {Modal} - Return modal instance.
   */
  openModal(component: any, data?: any) {
    const modal = this.modalController.create(component, data);
    modal.present();
    return modal;
  }

  /**
   * Open alert dialog.
   * @param {AlertOptions} passedOptions
   * @returns {Alert} - Return alert instance.
   */
  openAlert(passedOptions: AlertOptions) {
    const defaultOptions = {};

    const options = {
      ...defaultOptions,
      ...passedOptions
    };

    const alert = this.alertController.create(options);
    alert.present();
    return alert;
  }
}