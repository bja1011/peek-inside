export interface BoxItem {
  id?: number;
  boxId?: number;
  name: string;
  photo?: string;
  quantity: number;
}
