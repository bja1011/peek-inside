import {BoxItem} from "./box-item.model";

export interface Box {
  id: number;
  name: String;
  location?: String,
  items?: BoxItem[];
}
