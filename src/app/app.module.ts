import {NgModule, ErrorHandler} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {TabsPage} from '../pages/tabs/tabs';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {QRScannerModule} from "../modules/qr-scanner/qr-scanner.module";
import {ScanPage} from "../pages/scan/scan";
import {IonicStorageModule} from "@ionic/storage";
import {StorageService} from "../providers/storage/storage";
import {BrowsePage} from "../pages/browse/browse";
import {BoxesService} from "../providers/boxes/boxes";
import {BoxViewPage} from "../pages/box-view/box-view";
import {ModalService} from "../core/providers/modal.service";
import {BoxCreatePage} from "../pages/box-create/box-create";
import {ReactiveFormsModule} from "@angular/forms";
import {CreateBoxItemComponent} from "../components/create-box-item/create-box-item";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {CAMERA_GET_PICTURE_MOCK} from "../constants/mocks";
import {PipesModule} from "../pipes/pipes.module";
import {SettingsPage} from "../pages/contact/settings";

/**
 * Extend Camera class and override scan method to mock data if browser platform.
 * @todo: implement html5 camera.
 */
export class MockCamera extends Camera {

  getPicture(options?: CameraOptions): Promise<any> {
    return new Promise((resolve) => {
      resolve(CAMERA_GET_PICTURE_MOCK)
    });
  }
}

const isCordovaApp = document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1;

@NgModule({
  declarations: [
    MyApp,
    ScanPage,
    SettingsPage,
    BrowsePage,
    TabsPage,
    BoxViewPage,
    BoxCreatePage,
    CreateBoxItemComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios'
    }),
    IonicStorageModule.forRoot({
      name: '__pidb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    QRScannerModule,
    ReactiveFormsModule,
    PipesModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ScanPage,
    SettingsPage,
    BrowsePage,
    TabsPage,
    BoxViewPage,
    BoxCreatePage,
    CreateBoxItemComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StorageService,
    BoxesService,
    ModalService,
    {
      provide: Camera,
      useClass: isCordovaApp ? Camera : MockCamera,
    }

  ]
})
export class AppModule {
}
