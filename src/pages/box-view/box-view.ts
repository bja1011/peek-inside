import {Component} from '@angular/core';
import {NavController, NavParams, PopoverController} from 'ionic-angular';
import {Box} from "../../core/models/box.model";
import {ModalService} from "../../core/providers/modal.service";
import {BoxCreatePage} from "../box-create/box-create";
import {BoxItem} from "../../core/models/box-item.model";
import {BoxesService} from "../../providers/boxes/boxes";

@Component({
  selector: 'page-box-view',
  templateUrl: 'box-view.html',
})
export class BoxViewPage {

  box: Box;
  createBoxItemMode: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalService: ModalService,
              private popoverController: PopoverController,
              private boxesService: BoxesService,) {

    this.box = this.navParams.get('box');
    if (!this.box.items) this.box.items = [];
  }

  /**
   * Open box-create component with box object passed.
   * With box passed create-component will work in edit box mode.
   * @param {Box} box
   */
  goToEditBox(box: Box) {
    const modal = this.modalService.openModal(BoxCreatePage, {box});

    // Handle create-box modal change and refresh box data.
    modal.onDidDismiss(() => {
      this.boxesService.getBox(this.box.id)
        .then(box => {
          this.box = box;
        })
    })
  }

  openItemDetails(item: BoxItem) {
    console.log(item);
    //@todo: open item details.
  }

  /**
   * Handle box object emitted as output from create-box-item component.
   * @param {BoxItem} boxItem
   */
  handleBoxItemCreated(boxItem: BoxItem) {
    this.box.items.push(boxItem);
    this.boxesService.storeBox(this.box)
      .then(() => {
        this.createBoxItemMode = false;
      })
  }

  close() {
    return this.navCtrl.pop();
  }

}
