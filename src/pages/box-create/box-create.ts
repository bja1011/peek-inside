import {Component, OnInit} from '@angular/core';
import {NavController, NavParams, ToastController} from 'ionic-angular';
import {Box} from "../../core/models/box.model";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {QRScannerService} from "../../modules/qr-scanner/providers/qr-scanner.service";
import {BoxesService} from "../../providers/boxes/boxes";
import {ModalService} from "../../core/providers/modal.service";
import {BoxViewPage} from "../box-view/box-view";

@Component({
  selector: 'page-box-create',
  templateUrl: 'box-create.html',
})
export class BoxCreatePage implements OnInit {

  createBoxForm: FormGroup;
  submitted: boolean;
  editMode: boolean = false;
  passedBox: Box;
  locations: string[] = [];

  constructor(public navController: NavController,
              public navParams: NavParams,
              private qrScannerService: QRScannerService,
              private boxesService: BoxesService,
              private modalService: ModalService,
              private toastController: ToastController) {
  }

  /**
   * Scan QR code and assign it to box `id` property.
   */
  assignQRCodeId(): void {
    this.qrScannerService.scan()
      .then(scanResult => {

          // Search in storage for box with scanned id.
          this.boxesService.getBox(parseInt(scanResult.text))
            .then(box => {

              // If box found show alert to user.
              if (box) {
                this.modalService.openAlert(
                  {
                    title: 'Box exists',
                    subTitle: `You have used this QR code for box named <b>${box.name}</b>`,
                    buttons: ['OK']
                  }
                )
                // If box not found in storage update form `id` value.
              } else {
                this.createBoxForm.get('boxId').patchValue(scanResult.text);
              }
            })
        },
        (err) => {
          if (err.error == 'no-pattern') {
            const alert = this.modalService.openAlert({
              title: err.errorText,
              subTitle: 'Correct boxId format is: <br> \`pibox-{id}\`',
              buttons: ['OK']
            });
            alert.present();
          }
        });
  }

  /**
   * Save box in storage.
   */
  storeBox() {
    this.submitted = true;

    if (this.createBoxForm.valid) {
      let box: Box = {
        id: this.createBoxForm.value.boxId,
        name: this.createBoxForm.value.boxName,
        location: this.createBoxForm.value.boxLocation,
      };

      // If box passed in navParams (assign in ngOnInit) get items from it.
      if (this.passedBox) {
        box.items = this.passedBox.items;
      }

      // Save box in storage
      this.boxesService.storeBox(box)
        .then(() => {
          let toast = this.toastController.create({
            message: 'Saved successfully',
            duration: 3000
          });
          toast.present();

          // Close current modal and if current mode is not editing box open box details modal.
          this.close()
            .then(() => {
              if (!this.editMode) {
                this.modalService.openModal(BoxViewPage, {box});
              }
            })
        })
    }
  }

  /**
   * Handle select value changed.
   * If new value is `add-new` open prompt with input.
   * @param {string} event - New value from select component.
   */
  onChange(event: string) {
    if (event === 'add-new') {
      this.createBoxForm.get('boxLocation').patchValue(null);
      this.modalService.openAlert({
        title: 'Add new location',
        inputs: [
          {
            name: 'location',
            placeholder: 'location name'
          }
        ],
        buttons: [
          {
            text: 'Add',
            handler: (data) => {
              this.boxesService.storeBoxLocation(data.location)
                .then((locations: string[]) => {
                  this.locations = locations;
                })
            }
          }
        ]
      })
    }
  }

  ngOnInit() {

    this.boxesService.getBoxLocations()
      .then(locations => this.locations = locations);

    let box: Box = {
      id: null,
      name: null,
      location: null,
    };

    const passedBox = this.navParams.get('box');
    if (passedBox) {
      box = passedBox;
      this.passedBox = box;
      this.editMode = true;
    }

    const passedScanData = this.navParams.get('scanData');
    if (passedScanData) {
      box.id = passedScanData.text;
    }

    this.createBoxForm = new FormGroup({
      boxName: new FormControl(box.name, Validators.required),
      boxLocation: new FormControl(box.location, Validators.required),
      boxId: new FormControl(box.id, Validators.required),
    });
  }

  close() {
    return this.navController.pop();
  }
}