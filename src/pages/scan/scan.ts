import {Component} from '@angular/core';
import {QRScannerService} from "../../modules/qr-scanner/providers/qr-scanner.service";
import {BoxesService} from "../../providers/boxes/boxes";
import {Box} from "../../core/models/box.model";
import {AlertController, ModalController} from "ionic-angular";
import {BoxViewPage} from "../box-view/box-view";
import {BoxCreatePage} from "../box-create/box-create";
import {BarcodeScanResult} from "@ionic-native/barcode-scanner";

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html'
})
export class ScanPage {

  constructor(private qRScannerService: QRScannerService,
              private boxesService: BoxesService,
              private modalController: ModalController,
              private alertController: AlertController,) {
  }

  /**
   * Open scanner and handle returned promise.
   * Open box details if box found with scanner QR code id.
   * Open prompt to create new box if box not found.
   */
  openScanner(): void {

    //@todo: remove
    // [0, 0, 0, 0, 0, 0, 0, 0, 0].map((el, i) => {
    //   this.boxesService.storeBox(i, {
    //       id: i,
    //       name: 'test',
    //       items: [],
    //       photo: 'assets/imgs/scan-info.jpg'
    //     }
    //   );
    // });

    this.qRScannerService.scan({
      showTorchButton: true
    })
      .then(
        (scanData) => {

          // If scanner not cancelled by user
          if (!scanData.cancelled) {
            this.boxesService.getBox(parseInt(scanData.text))
              .then((box: Box) => {
                if (box) {
                  let modal = this.modalController.create(BoxViewPage, {box});
                  modal.present();
                } else {
                  this.showNoBoxFoundPrompt(scanData);
                }
              })
          }
        },
        (err) => {
          if (err.error == 'no-pattern') {
            const alert = this.alertController.create({
              title: err.errorText,
              subTitle: 'Correct boxId format is: <br> \`pibox-{id}\`',
              buttons: ['OK']
            });
            alert.present();
          }
          console.log(err);
          //@todo: handle scanner error.
        })
  }

  /**
   * Show prompt to create new box.
   * @param {BarcodeScanResult} scanData
   */
  showNoBoxFoundPrompt(scanData: BarcodeScanResult): void {
    let alert = this.alertController.create({
      title: 'Box not found',
      subTitle: 'Create new?',
      buttons: [
        {
          text: 'Create',
          handler: () => {
            const modal = this.modalController.create(BoxCreatePage, {scanData});
            modal.present();
          }
        },
        'Cancel'
      ]
    });
    alert.present();
  }
}

