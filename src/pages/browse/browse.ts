import {Component} from '@angular/core';
import {BoxesService} from "../../providers/boxes/boxes";
import {Box} from "../../core/models/box.model";
import {ModalService} from "../../core/providers/modal.service";
import {BoxViewPage} from "../box-view/box-view";
import {BoxCreatePage} from "../box-create/box-create";

@Component({
  selector: 'browse-home',
  templateUrl: 'browse.html'
})
export class BrowsePage {

  boxes: Box[];

  constructor(private boxesService: BoxesService,
              private modalService: ModalService,) {

    this.boxesService.getAllBoxes()
      .then((boxes: Box[]) => {
        this.boxes = boxes;
      })
  }

  openAddBoxModal() {
    this.modalService.openModal(BoxCreatePage);
  }

  openBoxDetailsModal(box: Box) {
    this.modalService.openModal(BoxViewPage, {box});
  }
}
