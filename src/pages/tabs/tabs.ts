import {Component} from '@angular/core';

import {ScanPage} from "../scan/scan";
import {BrowsePage} from "../browse/browse";
import {SettingsPage} from "../contact/settings";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = BrowsePage;
  tab2Root = ScanPage;
  tab3Root = SettingsPage;

  constructor() {
  }
}
