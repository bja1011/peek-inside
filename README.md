# Peek Inside Prototype

Quick Peek clone app prototype.

### Installing

Install Ionic CLI and Cordova (for android/ios build):

```
npm i -g ionic cordova
```

Install npm dependencies:

```
npm i
```

Prepare cordova platforms:

```
ionic cordova prepare
```

### Development

Run ionic dev server:

```
ionic serve
```

## Sample QR codes:
[QR Codes](https://ibb.co/dzSg3c) - See sample test QR codes. Value in QR code is `text`. Format:  `pibox-{id}`: 

## Deployment

```
ionic cordova build android --prod
```

For more see Ionic docs: 
[Ionic docs](https://ionicframework.com/docs/intro/deploying/) - Ionic deploy docs.

## Generate docs

```
npm run doc
```

## Demo video

[App Preview](https://www.youtube.com/watch?v=A_G-YjvgCDg)

## Todo: 
- generate QR codes from application (for print)
- items and boxes manage (delete item/box, transfer items between boxes etc.)
- better UI/UX
- integrate data synchronization with backend
- implement users authentication
- implement state management with RxJS Store
- implement html5 camera and qr scanner to allow run in browser (currently mocked on browser platform)
- extend storage service to store files on device or backend server when run in browser
- implement Crosswalk WebView for android